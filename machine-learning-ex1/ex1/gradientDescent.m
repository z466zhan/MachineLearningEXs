function [theta, J_history] = gradientDescent(X, y, theta, alpha, num_iters)
%GRADIENTDESCENT Performs gradient descent to learn theta
%   theta = GRADIENTDESCENT(X, y, theta, alpha, num_iters) updates theta by 
%   taking num_iters gradient steps with learning rate alpha

% Initialize some useful values
m = length(y); % number of training examples
J_history = zeros(num_iters, 1);
sizex = size(X); %record size of x
num_x = sizex(:,2); %number of colums in X
for iter = 1:num_iters

    % ====================== YOUR CODE HERE ======================
    % Instructions: Perform a single gradient step on the parameter vector
    %               theta. 
    %
    % Hint: While debugging, it can be useful to print out the values
    %       of the cost function (computeCost) and gradient here.
    %
    diff = (X*theta - y); %difference between Xtheta and Y
    %update theta simultaneously
    for i = 1:num_x
        slope = (alpha/m) * diff' * X(:,i); 
        %calculate the decreasing or increasing slope
        %from the given algorithm
        theta(i) = theta(i) - slope;
    end



    % ============================================================
    
    % Save the cost J in every iteration    
    J_history(iter) = computeCost(X, y, theta);

end

end
